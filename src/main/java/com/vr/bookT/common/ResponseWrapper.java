package com.vr.bookT.common;

import java.util.ArrayList;
import java.util.List;

public class ResponseWrapper<T> {

	private int code;
	private String status;
	private String message;
	private List<T> objects;

	public ResponseWrapper() {

	}

	public ResponseWrapper(int code, String status, String message) {
		this.code = code;
		this.status = status;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<T> getObjects() {
		return objects;
	}

	public void setObjects(List<T> objects) {
		this.objects = objects;
	}

	public void addObject(T obj) {
		if (this.objects == null)
			this.objects = new ArrayList<T>();
		objects.add(obj);
	}

}
