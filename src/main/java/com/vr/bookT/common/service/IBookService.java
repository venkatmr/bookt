package com.vr.bookT.common.service;

import com.vr.bookT.common.ResponseWrapper;

public interface IBookService<T> {
	
	ResponseWrapper<T> save(T Object);

	 ResponseWrapper<T> findAll();
}
