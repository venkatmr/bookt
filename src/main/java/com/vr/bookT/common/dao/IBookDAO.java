package com.vr.bookT.common.dao;

import java.util.List;

public interface IBookDAO<T> {

	public void save(T obj);
	
	public List<T> findAll();
	
	public T findByColumn(String[] column, Object[] value,String condition);
}
