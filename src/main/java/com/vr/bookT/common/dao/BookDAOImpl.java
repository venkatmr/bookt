package com.vr.bookT.common.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

public abstract class BookDAOImpl<T> implements IBookDAO<T> {

	private Class<T> clazz;


	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Autowired
	private HibernateTemplate template;


	@Override
	public void save(T obj) {
		if (obj != null)
			template.save(obj);
	}

	@Override
	public List<T> findAll() {
		return template.loadAll(clazz);		
	}

	@SuppressWarnings("unchecked")
	@Override
	public  T findByColumn(String[] columns, Object[] values, String condition){
		if(columns.length!=values.length) {
			return null;
		}
		StringBuilder builder = new StringBuilder();
		builder.append(" From " + clazz.getName() + " where 1=1 and ");
		int cl = columns.length;
		for (int i = 0; i < cl; i++) {
				builder.append(" ");
				builder.append(columns[i]).append(" = ").append( "?");
				builder.append(condition);
		}
		int lastIndex = builder.lastIndexOf(condition);
		return (T) (template.find((builder.substring(0, lastIndex)), values).get(0));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public  List<T> findByColumn(String columns, Object values){
		
		StringBuilder builder = new StringBuilder();
		builder.append(" From " + clazz.getName() + " where 1=1 and ");
				builder.append(" ");
				builder.append(columns).append(" = ").append( "?");
		return  (List<T>) (template.find(builder.toString(), values));
	}

		}
