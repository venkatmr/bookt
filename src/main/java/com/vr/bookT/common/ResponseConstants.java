package com.vr.bookT.common;

public interface ResponseConstants {

	int SUCCESS_CODE = 0;
	int FAILURE_CODE = 1;
	String SUCCESS_STATUS = "ok";
	String FAILURE_STATUS = "fail";
	String SUCCESS_MESSAGE = "success";
	String FAILURE_MESSAGE = "failed";

}
