package com.vr.bookT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookTApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookTApplication.class, args);
	}
}
