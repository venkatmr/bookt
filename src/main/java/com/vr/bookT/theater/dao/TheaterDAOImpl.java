package com.vr.bookT.theater.dao;

import org.springframework.stereotype.Repository;

import com.vr.bookT.common.dao.BookDAOImpl;
import com.vr.bookT.model.Theater;

@Repository
public class TheaterDAOImpl extends BookDAOImpl<Theater> {

	public TheaterDAOImpl() {
		setClazz(Theater.class);
	}

}
