package com.vr.bookT.theater.service;

import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.model.Theater;

public interface TheaterService {

	ResponseWrapper<Theater> save(Theater user);

	 ResponseWrapper<Theater> findAll();
}
