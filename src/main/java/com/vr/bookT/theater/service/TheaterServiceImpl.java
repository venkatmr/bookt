package com.vr.bookT.theater.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vr.bookT.common.ResponseConstants;
import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.common.dao.IBookDAO;
import com.vr.bookT.model.Theater;

@Service
@Transactional
public class TheaterServiceImpl implements TheaterService{

	private static Logger logger = LogManager.getLogger(TheaterServiceImpl.class);
	@Autowired
	private IBookDAO<Theater> theaterDAO;
	
	@Override
	public ResponseWrapper<Theater> save(Theater obj) {
		ResponseWrapper<Theater> response = new ResponseWrapper<>();
		try {
			theaterDAO.save(obj);
			response.setCode(ResponseConstants.SUCCESS_CODE);
			response.setStatus(ResponseConstants.SUCCESS_STATUS);
			response.setMessage(ResponseConstants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			logger.info(e.getMessage());
			response.setCode(ResponseConstants.FAILURE_CODE);
			response.setStatus(ResponseConstants.FAILURE_STATUS);
			response.setMessage(e.getMessage());
		}
		
		return response;
		
	}	

	@Override
	public ResponseWrapper<Theater> findAll() {
		ResponseWrapper<Theater> response = new ResponseWrapper<>();
		try {
			List<Theater> theaters = theaterDAO.findAll();
			if (!theaters.isEmpty()) {
				response.setCode(ResponseConstants.SUCCESS_CODE);
				response.setStatus(ResponseConstants.SUCCESS_STATUS);
				response.setMessage(ResponseConstants.SUCCESS_MESSAGE);
				response.setObjects(theaters);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			response.setCode(ResponseConstants.FAILURE_CODE);
			response.setStatus(ResponseConstants.FAILURE_STATUS);
			response.setMessage(e.getMessage());
		}
		return response;
	}

}
