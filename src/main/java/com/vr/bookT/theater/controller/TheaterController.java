package com.vr.bookT.theater.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.model.Theater;
import com.vr.bookT.theater.service.TheaterService;

@RestController
public class TheaterController {

	private static Logger logger = LogManager.getLogger(TheaterController.class);
	
	@Autowired
	private TheaterService theaterService;

	@RequestMapping(value = "/theaters")
	public ResponseWrapper<Theater> get() {

		logger.info("In get theaters");

		return theaterService.findAll();

	}

	@RequestMapping(value = "/theaters", method = RequestMethod.POST)
	public ResponseWrapper<Theater> addUser(@RequestBody Theater user) {

		return theaterService.save(user);
	}

}
