package com.vr.bookT.movie.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.model.Movie;
import com.vr.bookT.movie.service.MovieService;

@RestController
public class MovieController {

	private static Logger logger = LogManager.getLogger(MovieController.class);
	
	@Autowired
	private MovieService movieService;

	@RequestMapping(value = "/movies")
	public ResponseWrapper<Movie> get() {
		logger.info("In get theaters");

		return movieService.findAll();
	}

	@RequestMapping(value = "/movies", method = RequestMethod.POST)
	public ResponseWrapper<Movie> addMovie(@RequestBody Movie movie) {

		return movieService.save(movie);
	}
	
}
