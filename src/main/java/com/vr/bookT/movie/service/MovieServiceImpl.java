package com.vr.bookT.movie.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vr.bookT.common.IConstans;
import com.vr.bookT.common.ResponseConstants;
import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.common.dao.IBookDAO;
import com.vr.bookT.model.Movie;

@Service
@Transactional
public class MovieServiceImpl implements MovieService {
	
	private static Logger logger = LogManager.getLogger(MovieServiceImpl.class);
	
	@Autowired
	private IBookDAO<Movie> movieDAO;
	
	@Override
	public ResponseWrapper<Movie> save(Movie obj) {
		ResponseWrapper<Movie> response = new ResponseWrapper<>();
		try {
			obj.setCreatedBy(IConstans.USER_NAME); // useer name hardcoded for now TODO
			obj.setCreatedDate(new Date()); // set date
			movieDAO.save(obj);
			response.setCode(ResponseConstants.SUCCESS_CODE);
			response.setStatus(ResponseConstants.SUCCESS_STATUS);
			response.setMessage(ResponseConstants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			logger.info(e.getMessage());
			response.setCode(ResponseConstants.FAILURE_CODE);
			response.setStatus(ResponseConstants.FAILURE_STATUS);
			response.setMessage(e.getMessage());
		}
		
		return response;
		
	}

	@Override
	public ResponseWrapper<Movie> findAll() {
		ResponseWrapper<Movie> response = new ResponseWrapper<>();
		try {
			List<Movie> movies = movieDAO.findAll();
			if (!movies.isEmpty()) {
				response.setCode(ResponseConstants.SUCCESS_CODE);
				response.setStatus(ResponseConstants.SUCCESS_STATUS);
				response.setMessage(ResponseConstants.SUCCESS_MESSAGE);
				response.setObjects(movies);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			response.setCode(ResponseConstants.FAILURE_CODE);
			response.setStatus(ResponseConstants.FAILURE_STATUS);
			response.setMessage(e.getMessage());
		}
		return response;
	}



}
