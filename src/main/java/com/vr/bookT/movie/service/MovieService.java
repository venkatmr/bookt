package com.vr.bookT.movie.service;

import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.model.Movie;

public interface MovieService {
	ResponseWrapper<Movie> save(Movie user);

	 ResponseWrapper<Movie> findAll();
	 
	 
}
