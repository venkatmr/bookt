package com.vr.bookT.movie.dao;

import org.springframework.stereotype.Repository;

import com.vr.bookT.common.dao.BookDAOImpl;
import com.vr.bookT.model.Movie;

@Repository
public class MovieDAO extends BookDAOImpl<Movie> {

	public MovieDAO() {
		setClazz(Movie.class);
	}
}
