package com.vr.bookT.show.dao;

import org.springframework.stereotype.Repository;

import com.vr.bookT.common.dao.BookDAOImpl;
import com.vr.bookT.model.Shows;

@Repository
public class ShowDAO extends BookDAOImpl<Shows> {

	public ShowDAO() {
		setClazz(Shows.class);
	}
}
