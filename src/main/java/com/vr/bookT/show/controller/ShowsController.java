package com.vr.bookT.show.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.model.Shows;
import com.vr.bookT.show.service.ShowService;

@RestController
public class ShowsController {

	private static Logger logger = LogManager.getLogger(ShowsController.class);
	@Autowired
	private ShowService showsService;

	@RequestMapping(value = "/shows")
	public ResponseWrapper<Shows> get() {

		return showsService.findAll();
	}
	
	@RequestMapping(value = "/shows/{theaterId}")
	public ResponseWrapper<Shows> getShowsByTheater(@PathVariable(name="theaterId") Integer theaterId) {
		logger.info("In Get Shows by theater : "+theaterId);

		return showsService.findByTheater(theaterId);
	}

	@RequestMapping(value = "/shows", method = RequestMethod.POST)
	public ResponseWrapper<Shows> addMovie(@RequestBody Shows show) {

		return showsService.save(show);
	}
}
