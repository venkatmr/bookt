package com.vr.bookT.show.service;

import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.model.Shows;

public interface ShowService {

	ResponseWrapper<Shows> save(Shows user);

	 ResponseWrapper<Shows> findAll();

	ResponseWrapper<Shows> findByTheater(int theaterId);
	 
}
