package com.vr.bookT.show.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vr.bookT.common.ResponseConstants;
import com.vr.bookT.common.ResponseWrapper;
import com.vr.bookT.common.dao.IBookDAO;
import com.vr.bookT.model.Shows;

@Service
@Transactional
public class ShowsServiceImpl implements ShowService {
	
	private static Logger logger = LogManager.getLogger(ShowsServiceImpl.class);
	
	@Autowired
	private IBookDAO<Shows> showsDAO;
	
	@Override
	public ResponseWrapper<Shows> save(Shows show) {
		ResponseWrapper<Shows> response = new ResponseWrapper<>();
		try {
			showsDAO.save(show);
			response.setCode(ResponseConstants.SUCCESS_CODE);
			response.setStatus(ResponseConstants.SUCCESS_STATUS);
			response.setMessage(ResponseConstants.SUCCESS_MESSAGE);
		} catch (Exception e) {
			logger.info(e.getMessage());
			response.setCode(ResponseConstants.FAILURE_CODE);
			response.setStatus(ResponseConstants.FAILURE_STATUS);
			response.setMessage(e.getMessage());
		}
		
		return response;
		
	}

	@Override
	public ResponseWrapper<Shows> findAll() {
		ResponseWrapper<Shows> response = new ResponseWrapper<>();
		try {
			List<Shows> shows = showsDAO.findAll();
			if (!shows.isEmpty()) {
				response.setCode(ResponseConstants.SUCCESS_CODE);
				response.setStatus(ResponseConstants.SUCCESS_STATUS);
				response.setMessage(ResponseConstants.SUCCESS_MESSAGE);
				response.setObjects(shows);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			response.setCode(ResponseConstants.FAILURE_CODE);
			response.setStatus(ResponseConstants.FAILURE_STATUS);
			response.setMessage(e.getMessage());
		}
		return response;
	}
	@Override
	public ResponseWrapper<Shows> findByTheater(int theaterId) {

		ResponseWrapper<Shows> response = new ResponseWrapper<>();
		try {
			List<Shows> movies = showsDAO.findByColumn("theater_id", theaterId);
			response.setCode(ResponseConstants.SUCCESS_CODE);
			response.setStatus(ResponseConstants.SUCCESS_STATUS);
			response.setMessage(ResponseConstants.SUCCESS_MESSAGE);
			if (!movies.isEmpty()) {
				response.setObjects(movies);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			response.setCode(ResponseConstants.FAILURE_CODE);
			response.setStatus(ResponseConstants.FAILURE_STATUS);
			response.setMessage(e.getMessage());
		}
		return response;
	
	}
}
